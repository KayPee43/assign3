package edu.towson.cosc435.pegram.todos2

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import edu.towson.cosc435.pegram.todos2.ui.theme.Todos2Theme

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Todos2Theme {
                Scaffold(
                    topBar = { MyTopAppBar() }
                ) {

                }
            }
        }
    }
}
@Preview
@Composable
fun MyTopAppBar(){
    TopAppBar(
        title = { Text(text = "Todos")},
        backgroundColor = Color.Red,
        contentColor = Color.White,
        navigationIcon = {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(Icons.Default.Menu, "Menu")
            }
        },
        actions = {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(Icons.Default.Settings, "Settings")
            }
        }
    )
}